# Custom Search Command - Load JSON From API

Generating command that pulls events from an api.

## Usage

- Copy splunklib to lib folder if not installed globally [splunk-sdk-python](https://github.com/splunk/splunk-sdk-python)
- Install the app
- Run `| loadjsonfromapi api_url=<api_url>` to load json from the api

## Example
- Use the spl `| loadjsonfromapi api_url="https://official-joke-api.appspot.com/random_ten"` to load from the api.
![Screenshot](docs/images/spl.png)
