
import requests
import time

def generate(api_url):

    response = requests.get(api_url)
    data = response.json()

    if isinstance(data, list):
        for record in data:
            record.update({'_time': time.time()})
            print(record)
    else:
        data.update({'_time': time.time()})
        print(data)

print("="*100)
generate("https://official-joke-api.appspot.com/random_joke")
print("="*100)
generate("https://official-joke-api.appspot.com/random_ten")
print("="*100)
