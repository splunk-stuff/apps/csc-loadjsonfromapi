#!/usr/bin/env python
# coding=utf-8

import os, sys
import time
import requests
# import json

sys.path.insert(0, os.path.join(os.path.dirname(__file__), "..", "lib"))
from splunklib.searchcommands import dispatch, GeneratingCommand, Configuration, Option, validators

@Configuration()
class LoadJSON(GeneratingCommand):

    """ Generates records drawn from a json file.

    ##Syntax

    .. code-block::
        loadjsonfromapi api_url=<url>

    ##Description

    The :code:`loadjsonfromapi` command loads :code:`json` records from an api.

    ##Example

    .. code-block::
        | loadjsonfromapi api_url="https://official-joke-api.appspot.com/random_ten"

    This example generates records from the supplied api.

    """

    api_url = Option(
        doc='''**Syntax:** **api_url=***<url>*
        **Description:** The url of the api to call''',
        name='api_url', require=True)

    def generate(self):

        response = requests.get(self.api_url)
        data = response.json()

        if isinstance(data, list):
            for record in data:
                record.update({'_time': time.time()})
                yield record
        else:
            data.update({'_time': time.time()})
            yield data

dispatch(LoadJSON, sys.argv, sys.stdin, sys.stdout, __name__)
